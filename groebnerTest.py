from sage.all import *
from sage.manifolds.operators import *
import copy

def build_ring(numVars, numDualVars):
    alphabet = "abcdefghijkplmnopqrstuvwxyz"
    varsLabels = alphabet[:numVars]
    dualVarsLabels = alphabet[-numDualVars:]
    return PolynomialRing(QQbar, numVars + numDualVars, varsLabels + dualVarsLabels, order='lex')

def build_lagrangian(R, f, P):
    varsList = R.gens()
    L = f + sum([R.gens()[-(i + 1)] * p for (i, p) in enumerate(P)])
    return L
    
def back_solve_solver(R, Eqs, priorSols):
    i = 1
    eq = Eqs[-i]
    while eq.is_constant():
        i = i + 1
        if i > len(list(Eqs)):
            return {}
        eq = Eqs[-i]

    if not eq.is_univariate():
        raise Exception("System Not Triangular")
    
    for a in eq.dict().keys():
        if sum(a) == 0:
            continue
        i = list(map(lambda x: x != 0, a)).index(True)
        break

    v = R.gens()[i]

    roots = eq.univariate_polynomial().roots()

    sols_post = []
    for r, _ in roots:
        sol_tmp = copy.deepcopy(priorSols)
        sol_tmp[v] = r
        sols_post.append(sol_tmp)

    return sols_post

def back_solve_step(R, Eqs, priorSols):
    if len(priorSols) == 0:
        return back_solve_solver(R, Eqs, {})

    newSols = []
    for s in priorSols:
        new_sols_partial = back_solve_solver(R, Eqs.subs(s), s)
        newSols.extend(new_sols_partial)
    return newSols

def back_solve(R, Eqs):
    sols = []
    for _ in range(R.ngens()):
        sols = back_solve_step(R, Eqs, sols)
    return sols

def print_eqs(Eqs):
    for eq in Eqs:
        print(eq)

if __name__ == "__main__":
    # Example Optimization Problem 

    numVars = 3
    numDualVars = 2

    R = build_ring(numVars, numDualVars)
    v = R.gens()
    f = v[0] * v[1]**2 * v[2]
    P = [-6 + v[0]**2 + v[1]**2 + v[2]**2,
        v[0] + v[1] - v[2]]

    L = build_lagrangian(R, f, P)
    grad_L = grad(L)
    grad_L_ideal = grad_L * R
    gb_grad_L = grad_L_ideal.groebner_basis()

    print("minimize ", f)
    print("subject to: ")
    for p in P:
        print(p, "= 0")

    print()
    print("Lagrangian = ", L)

    print()
    print("Gradient of Lagrangian = ", grad_L)

    print()
    print("Grobner Basis of Lagranian Problem: ")
    print_eqs(gb_grad_L)

    print()
    sols = back_solve(R, gb_grad_L)

    f_eval = [f.subs(s) for s in sols]
    f_min = min(f_eval)
    arg_min = [s for s in sols if f.subs(s) == f_min]
    print()
    print("minimun value: ", f_min)
    print("minimizing solutions: ", arg_min)
