# Optimization and Algebra

A prototype implementation using SageMath for solving polynomial optimization problems using Grobner basis. Example taken from "Polynomial programming using Groebner basis" by Chang and Wah. 

## Getting started

Install SageMath (https://doc.sagemath.org/html/en/installation/index.html).

Run command:
```
sage -python groebnerTest.py
```

Script Output:
```
minimize  a*b^2*c
subject to: 
a^2 + b^2 + c^2 - 6 = 0
a + b - c = 0

Lagrangian =  a^2*z + a*b^2*c + a*y + b^2*z + b*y + c^2*z - c*y + (-6)*z

Gradient of Lagrangian =  [2*a*z + b^2*c + y, 2*a*b*c + 2*b*z + y, a*b^2 + 2*c*z - y, a + b - c, a^2 + b^2 + c^2 - 6]

Grobner Basis of Lagranian Problem: 
a - c + (-2/5)*y*z + (-29/30)*y
b + 2/5*y*z + 29/30*y
c^2 + 2/3*c*y + 2*z^2 + (-1/2)*z - 3
c*y*z + 3/4*c*y + z^2 + 3/4*z
c*z^2 + 3/4*c*z + y*z + 3/4*y
y^2 + (-14/5)*z^2 + 12/5*z
y*z^2 + (-7/12)*y*z - y
z^3 + (-7/12)*z^2 - z


minimun value:  -4
minimizing solutions:  [{z: 4/3, y: -4/3, c: 1, b: 2, a: -1}, {z: 4/3, y: 4/3, c: -1, b: -2, a: 1}]
```
